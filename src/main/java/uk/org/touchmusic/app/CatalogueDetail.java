package uk.org.touchmusic.app;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.*;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.github.kevinsawicki.http.HttpRequest;
import com.squareup.picasso.Picasso;
import uk.org.touchmusic.app.model.CatalogueItem;
import uk.org.touchmusic.app.model.ItemType;

import java.util.ArrayList;
import java.util.List;

public class CatalogueDetail extends BaseDetailFragment {

    private int idx;
    private String key;
    private String mp3;
    private Button playButton;
    private CatalogueItem catalogueItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.catalogue_item, container, false);

        idx = getArguments().getInt("idx");
        key = ItemType.CATALOGUE.name() + idx;

        List<CatalogueItem> catalogueItems = new ArrayList<CatalogueItem>();
        catalogueItems = contentProvider.getContent(ItemType.CATALOGUE.name(), catalogueItems.getClass());
        catalogueItem = catalogueItems.get(idx);
        mp3 = catalogueItem.getMp3sampleUrl();

        TextView title = (TextView) v.findViewById(R.id.title);
        TextView artist = (TextView) v.findViewById(R.id.artist);
        TextView releaseDate = (TextView) v.findViewById(R.id.releaseDate);
        ImageView coverArt = (ImageView) v.findViewById(R.id.coverArt);
        TextView trackListing = (TextView) v.findViewById(R.id.trackListing);
        TextView description = (TextView) v.findViewById(R.id.description);
        Button viewOnWeb = (Button) v.findViewById(R.id.viewOnWeb);

        title.setText(catalogueItem.getTitle());
        artist.setText(catalogueItem.getArtist());
        String releaseInfo = "Released: " + catalogueItem.getReleaseDate() + " | Catalogue #: " + catalogueItem.getCatalogueNumber();
        releaseDate.setText(releaseInfo);
        Picasso.with(getActivity().getBaseContext())
                .load(catalogueItem.getCoverArtUrl())
                .resize(600,600)
                .into(coverArt);
        trackListing.setText(catalogueItem.getTrackListing());
        description.setText(catalogueItem.getDescription());

        playButton = (Button) v.findViewById(R.id.playButton);
        if (TextUtils.isEmpty(mp3)) {
            // mp3 sample may not have been specified
            playButton.setVisibility(View.GONE);
        } else {
            // mp3 sample may not exist
            new FindMp3().execute(mp3);
        }

        viewOnWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(catalogueItem.getReleaseUrl()));
                startActivity(intent);
            }
        });

        // share a link to the release
        setHasOptionsMenu(true);
        shareSubject = catalogueItem.getArtist() + ": " + catalogueItem.getTitle();
        shareText = shareSubject + " - " + catalogueItem.getReleaseUrl();
        return v;
    }

    private class FindMp3 extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpRequest request = HttpRequest.get(params[0]);
                if (request.ok()) {
                    return true;
                }
            } catch (HttpRequest.HttpRequestException ex) {
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean mp3exists) {
            if (mp3exists) {
                playButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FrameLayout fl = (FrameLayout) getActivity().findViewById(R.id.playerContainer);
                    Player player = (Player) fm.findFragmentByTag("DETAIL_PLAYER");

                    // play mp3 and show details
                    if (player == null || !player.getKey().equals(key)) {
                        player = Player.getInstance(ItemType.CATALOGUE, idx);
                        fm.beginTransaction().replace(R.id.playerContainer, player, "DETAIL_PLAYER").setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
                        app.getPlayerService().play(mp3, catalogueItem.getTitle(), catalogueItem.getArtist());
                        app.setCurrentlyPlaying(mp3, ItemType.CATALOGUE, idx);
                    }

                    fl.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                playButton.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }
}
