package uk.org.touchmusic.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.*;

public class BaseDetailFragment extends Fragment {

    protected TouchMusicApplication app;
    protected ContentProvider contentProvider;
    protected String shareSubject;
    protected String shareText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        app = (TouchMusicApplication) getActivity().getApplication();
        contentProvider = app.getContentProvider();

        return super.onCreateView(inflater, container, savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // gets called on orientation change, so clear before adding to avoid duplicating
        menu.clear();
        inflater.inflate(R.menu.share_detail, menu);
        MenuItem shareItem = menu.findItem(R.id.menu_item_share);
        ShareActionProvider shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, shareSubject);
        intent.putExtra(Intent.EXTRA_TEXT, shareText);
        shareActionProvider.setShareIntent(intent);
    }
}
