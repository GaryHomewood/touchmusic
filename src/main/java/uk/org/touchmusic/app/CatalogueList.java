package uk.org.touchmusic.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.github.kevinsawicki.http.HttpRequest;
import com.squareup.picasso.Picasso;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import uk.org.touchmusic.app.model.CatalogueItem;
import uk.org.touchmusic.app.model.ItemType;

import java.util.ArrayList;
import java.util.List;

public class CatalogueList extends Fragment {

    private CatalogueListAdapter catalogueListAdapter;
    private ListView catalogueListView;
    private TouchMusicApplication app;
    private ContentProvider contentProvider;
    private ProgressBar loader;
    private Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        app = (TouchMusicApplication) getActivity().getApplication();
        contentProvider = app.getContentProvider();

        View v = inflater.inflate(R.layout.catalogue_list, container, false);
        catalogueListView = (ListView) v.findViewById(R.id.catalogueListView);

        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            // list header image
            View header = LayoutInflater.from(getActivity()).inflate(R.layout.listview_header, null);
            ImageView headerImage = (ImageView) header.findViewById(R.id.list_header_image);
            headerImage.setImageResource(R.drawable.catalogue_header);
            catalogueListView.addHeaderView(header);
        }

        loader = (ProgressBar) v.findViewById(R.id.loader);
        loader.setVisibility(View.GONE);

        refreshData();
        return v;
    }

    public interface OnCatalogueItemSelected {
        public void onCatalogueItemSelected(int idx);
    }

    OnCatalogueItemSelected listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        try {
            listener = (OnCatalogueItemSelected) activity;
        } catch (ClassCastException ex) {
        }
    }

    public void refreshData() {
        new GetCatalogue().execute();
    }

    private class GetCatalogue extends AsyncTask<String, Void, List<CatalogueItem>> {

        private static final String CATALOGUE_URL = "http://electric-window-7475.herokuapp.com/releases/publisher/Touch.xml";

        public GetCatalogue() {
        }

        @Override
        protected void onPreExecute() {
            loader.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<CatalogueItem> doInBackground(String... params) {
            List<CatalogueItem> catalogueItems = new ArrayList<CatalogueItem>();
            catalogueItems = contentProvider.getContent(ItemType.CATALOGUE.name(), catalogueItems.getClass());
            if (catalogueItems == null) {
                catalogueItems = doRequest(CATALOGUE_URL);
            }
            return catalogueItems;
        }

        private List<CatalogueItem> doRequest(String url) {

            HttpRequest request = HttpRequest.get(url);
            String response = request.body();

            List<CatalogueItem> catalogueItems = new ArrayList<CatalogueItem>();
            Document xml = XmlParser.stringToXml(response);
            NodeList releases = xml.getElementsByTagName("release");

            for (int i = 0; i < releases.getLength(); i++) {
                Element el = (Element) releases.item(i);

                CatalogueItem catalogueItem = new CatalogueItem();
                catalogueItem.setArtist(XmlParser.getValue(el, "artist"));
                catalogueItem.setTitle(XmlParser.getValue(el, "title"));
                catalogueItem.setReleaseDate(XmlParser.getValue(el, "release_date"));
                catalogueItem.setCatalogueNumber(XmlParser.getValue(el, "catalogue_number"));
                catalogueItem.setCoverArtUrl(XmlParser.getValue(el, "cover_art_url"));
                catalogueItem.setTrackListing(XmlParser.getValue(el, "track_listing"));
                catalogueItem.setMp3sampleUrl(XmlParser.getValue(el, "mp3_sample_url"));
                catalogueItem.setDescription(XmlParser.getValue(el, "description"));
                catalogueItem.setReleaseUrl(XmlParser.getValue(el, "release_url"));
                catalogueItems.add(catalogueItem);
            }

            contentProvider.setContent(ItemType.CATALOGUE.name(), catalogueItems);
            return catalogueItems;
        }

        @Override
        protected void onPostExecute(List<CatalogueItem> catalogueItems) {
            loader.setVisibility(View.GONE);

            catalogueListAdapter = new CatalogueListAdapter(activity, 0, catalogueItems);
            catalogueListView.setAdapter(catalogueListAdapter);
            catalogueListAdapter.notifyDataSetChanged();
        }
    }

    public class CatalogueListAdapter extends ArrayAdapter<CatalogueItem> {

        private List<CatalogueItem> catalogueItems;
        private Context context;

        public CatalogueListAdapter(Context context, int resource, List<CatalogueItem> catalogueItems) {
            super(context, resource, catalogueItems);
            this.catalogueItems = catalogueItems;
            this.context = context;
        }

        @Override
        public int getCount() {
            return catalogueItems.size();
        }

        @Override
        public CatalogueItem getItem(int position) {
            return catalogueItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;

            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                convertView = inflater.inflate(R.layout.catalogue_list_item, null);
                viewHolder = new ViewHolder();
                viewHolder.coverArt = (ImageView) convertView.findViewById(R.id.coverArt);
                viewHolder.artist = (TextView) convertView.findViewById(R.id.artist);
                viewHolder.title = (TextView) convertView.findViewById(R.id.title);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final CatalogueItem catalogueItem = catalogueItems.get(position);
            if (!TextUtils.isEmpty(catalogueItem.getCoverArtUrl())) {
                Picasso.with(this.context)
                    .load(catalogueItem.getCoverArtUrl())
                    .placeholder(R.drawable.list_square_placeholder)
                    .resize(app.getPxFromDpi(58), app.getPxFromDpi(58))
                    .into(viewHolder.coverArt);
            }
            viewHolder.artist.setText(catalogueItem.getArtist());
            viewHolder.title.setText(catalogueItem.getTitle());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCatalogueItemSelected(position);
                }
            });

            return convertView;
        }
    }

    static class ViewHolder {
        ImageView coverArt;
        TextView artist;
        TextView title;
    }
}
