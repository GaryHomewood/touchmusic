package uk.org.touchmusic.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

public class DetailActivity extends ActionBarActivity implements Player.OnPlayerRemove {

    private TouchMusicApplication app;
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_container);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        app = (TouchMusicApplication) getApplication();
        Bundle extras = getIntent().getExtras();
        String itemType = extras.getString("itemType");

        if (itemType.equals("news")) {
            fragment = Fragment.instantiate(this, NewsDetail.class.getName(), extras);
        } else if (itemType.equals("catalogue")) {
            fragment = Fragment.instantiate(this, CatalogueDetail.class.getName(), extras);
        } else if (itemType.equals("radio")) {
            fragment = Fragment.instantiate(this, RadioDetail.class.getName(), extras);
        } else if (itemType.equals("recipe")) {
            fragment = Fragment.instantiate(this, RecipeDetail.class.getName(), extras);
        }

        // add the appropriate fragment
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.containerDetail, fragment, itemType).commit();

        // show the player if an mp3 is playing
        FrameLayout fl = (FrameLayout) findViewById(R.id.playerContainer);
        if (!TextUtils.isEmpty(app.getCurrentlyPlaying())) {
            Player player = (Player) fm.findFragmentByTag("DETAIL_PLAYER");
            if (player == null) {
                player = Player.getInstance(app.getCurrentlyPlayingType(), app.getCurrentlyPlayingIdx());
                fm.beginTransaction().add(R.id.playerContainer, player, "DETAIL_PLAYER").commit();
            } else {
                fm.beginTransaction().replace(R.id.playerContainer, player, "DETAIL_PLAYER").commit();
            }

            fl.setVisibility(View.VISIBLE);
        } else {
            fl.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPlayerRemove() {
        app.removeCurrentlyPlaying();
        app.getPlayerService().stop();
        FragmentManager fm = getSupportFragmentManager();
        Fragment f = fm.findFragmentByTag("DETAIL_PLAYER");
        fm.beginTransaction().remove(f).commit();
        (findViewById(R.id.playerContainer)).setVisibility(View.GONE);
    }
}
