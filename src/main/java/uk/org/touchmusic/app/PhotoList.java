package uk.org.touchmusic.app;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.github.kevinsawicki.http.HttpRequest;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONObject;
import uk.org.touchmusic.app.model.FlickrPhoto;
import uk.org.touchmusic.app.model.ItemType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PhotoList extends Fragment {

    private ImageAdapter photGridAdapter;
    private GridView photoGrid;
    private OnPhotoSelectedListener listener;
    private TouchMusicApplication app;
    private ContentProvider contentProvider;
    private ProgressBar loader;
    private Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        app = (TouchMusicApplication) getActivity().getApplication();
        contentProvider = app.getContentProvider();

        View v = inflater.inflate(R.layout.photo_grid, container, false);
        photoGrid = (GridView) v.findViewById(R.id.photoGrid);

        loader = (ProgressBar) v.findViewById(R.id.loader);
        loader.setVisibility(View.GONE);

        refreshData();
        return v;
    }
        
    public interface OnPhotoSelectedListener {
        public void onPhotoSelected(int position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        try {
            listener = (OnPhotoSelectedListener) activity;
        } catch (ClassCastException ex) {
        }
    }

    public void refreshData() {
        new GetPhotos().execute();
    }

    private class GetPhotos extends AsyncTask<String, Void, List<FlickrPhoto>> {

        private final String PHOTO_URL = "https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos"
                + "&format=json"
                + "&photoset_id=72157627750718372"
                + "&api_key=dcb74491ec5cbe64deb98b18df1125a9"
                + "&extras=date_upload"
                + "&nojsoncallback=1";

        @Override
        protected void onPreExecute() {
            loader.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<FlickrPhoto> doInBackground(String... params) {
            List<FlickrPhoto> photos = new ArrayList<FlickrPhoto>();
            photos = contentProvider.getContent(ItemType.PHOTOS.name(), photos.getClass());
            if (photos == null || photos.size() == 0) {
                photos = doRequest(PHOTO_URL);
            }
            return photos;
        }

        private List<FlickrPhoto> doRequest(String url) {
            String response = HttpRequest.get(url).body();
            List<FlickrPhoto> photos = new ArrayList<FlickrPhoto>();

            try {
                JSONObject json = new JSONObject(response);
                JSONObject photoSet = json.getJSONObject("photoset");
                JSONArray photosJson = photoSet.getJSONArray("photo");

                for (int i = 0; i < photosJson.length(); i++) {
                    JSONObject jsonPhoto = photosJson.getJSONObject(i);
                    FlickrPhoto photo = new FlickrPhoto(jsonPhoto.getString("id"),
                                                        jsonPhoto.getString("secret"),
                                                        jsonPhoto.getString("server"),
                                                        jsonPhoto.getString("farm"),
                                                        jsonPhoto.getLong("dateupload"));
                    photos.add(photo);
                }
            } catch (Exception ex) {
            }

            // sort photos in date upload order
            Collections.sort(photos);

            // put results in the cache
            contentProvider.setContent(ItemType.PHOTOS.name(), photos);

            return photos;
        }

        @Override
        protected void onPostExecute(List<FlickrPhoto> photos) {
            loader.setVisibility(View.GONE);

            photGridAdapter = new ImageAdapter(activity, photos);
            photoGrid.setAdapter(photGridAdapter);
            photGridAdapter.notifyDataSetChanged();
        }
    }

    public class ImageAdapter extends BaseAdapter {
        private Context context;
        private List<FlickrPhoto> photos;

        public ImageAdapter(Context context, List<FlickrPhoto> photos) {
            this.context = context;
            this.photos = photos;
        }

        @Override
        public int getCount() {
            return photos.size();
        }

        @Override
        public Object getItem(int position) {
            return photos.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;

            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                convertView = inflater.inflate(R.layout.photo_grid_item, null);
                viewHolder = new ViewHolder();
                viewHolder.photo = (ImageView) convertView.findViewById(R.id.photo);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final FlickrPhoto photo = photos.get(position);

            // use a smaller image for <= hpdi
            String photoUrl;
            if (app.getDevice().getDensityDpi() > DisplayMetrics.DENSITY_HIGH) {
                photoUrl = photo.small240();
            } else {
                photoUrl = photo.largeSquare150();
            }

            int dp = (int) (getResources().getDimension(R.dimen.photo_grid_size)  / getResources().getDisplayMetrics().density);
            Picasso.with(this.context)
                    .load(photoUrl)
                    .resize(app.getPxFromDpi(dp), app.getPxFromDpi(dp))
                    .centerCrop()
                    .into(viewHolder.photo);

            viewHolder.photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPhotoSelected(position);
                }
            });
            return convertView;
        }

        private class ViewHolder {
            ImageView photo;
        }
    }
}
