package uk.org.touchmusic.app;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.github.kevinsawicki.http.HttpRequest;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import uk.org.touchmusic.app.model.ItemType;
import uk.org.touchmusic.app.model.Recipe;
import uk.org.touchmusic.app.model.RecipeGroup;

import java.util.ArrayList;
import java.util.List;

public class RecipeList extends Fragment {

    private RecipeListAdapter recipeListAdapter;
    private ExpandableListView recipeList;
    private OnRecipeSelected listener;
    private List<RecipeGroup> recipeGroups = new ArrayList<RecipeGroup>();
    private TouchMusicApplication app;
    private ContentProvider contentProvider;
    private ProgressBar loader;
    private Activity activity;

    public interface OnRecipeSelected {
        public void onRecipeSelected(Recipe recipe);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        try {
            listener = (OnRecipeSelected) activity;
        } catch (ClassCastException ex) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        app = (TouchMusicApplication) getActivity().getApplication();
        contentProvider = app.getContentProvider();

        View v = inflater.inflate(R.layout.recipe_list, container, false);

        recipeList = (ExpandableListView) v.findViewById(R.id.recipeList);
        recipeList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Recipe recipe = recipeGroups.get(groupPosition).getRecipes().get(childPosition);
                listener.onRecipeSelected(recipe);
                return false;
            }
        });

        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            // list header image
            View header = LayoutInflater.from(getActivity()).inflate(R.layout.listview_header, null);
            ImageView headerImage = (ImageView) header.findViewById(R.id.list_header_image);
            headerImage.setImageResource(R.drawable.recipes_header);
            recipeList.addHeaderView(header);
        }

        loader = (ProgressBar) v.findViewById(R.id.loader);
        loader.setVisibility(View.GONE);

        refreshData();
        return v;
    }

    public void refreshData() {
        new GetRecipes().execute();
    }

    private class GetRecipes extends AsyncTask<Void, Void, List<RecipeGroup>> {

        private static final String RECIPE_GROUPS = "http://www.touchmusic.org.uk/recipebook/categories.xml";

        @Override
        protected void onPreExecute() {
            loader.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<RecipeGroup> doInBackground(Void... params) {
            recipeGroups = contentProvider.getContent(ItemType.RECIPE.name(), recipeGroups.getClass());
            if (recipeGroups == null) {
                recipeGroups = doRequest();
            }
            return recipeGroups;
        }

        private List<RecipeGroup> doRequest() {
            List<RecipeGroup> recipeGroups = new ArrayList<RecipeGroup>();
            String response = HttpRequest.get(RECIPE_GROUPS).body();
            Document xml = XmlParser.stringToXml(response);
            NodeList items = xml.getElementsByTagName("category");

            for (int i = 0; i < items.getLength(); i++) {
                Element el = (Element) items.item(i);
                String groupName = XmlParser.getValue(el, "title");
                if (!groupName.toLowerCase().equals("left")) {
                    RecipeGroup recipeGroup = new RecipeGroup(groupName);

                    // get recipes for this group and add them to the group
                    List<Recipe> recipes = getRecipes(recipeGroup.getName());
                    recipeGroup.setRecipes(recipes);
                    recipeGroups.add(recipeGroup);
                }
            }

            // put results in the cache
            contentProvider.setContent(ItemType.RECIPE.name(), recipeGroups);
            return recipeGroups;
        }

        private List<Recipe> getRecipes(String groupName) {
            List<Recipe> recipes = new ArrayList<Recipe>();

            // convert recipe group to url safe form
            groupName = groupName.toLowerCase().replace(" ", "").replace("&", "and");
            int commaIdx = groupName.indexOf(",");
            if (commaIdx > 0) {
                groupName = groupName.substring(0, commaIdx);
            }
            final String url = ("http://www.touchmusic.org.uk/recipebook/" + groupName + ".xml");
            HttpRequest request = HttpRequest.get(url);

            if (request.ok()) {
                String response = request.body();
                if (!TextUtils.isEmpty(response)) {
                    Document xml = XmlParser.stringToXml(response);
                    NodeList items = xml.getElementsByTagName("item");

                    for (int i = 0; i < items.getLength(); i++) {
                        Element el = (Element) items.item(i);
                        Recipe recipe = new Recipe();
                        recipe.setTitle(XmlParser.getValue(el, "title"));
                        recipe.setExcerpt(XmlParser.getValue(el, "excerpt"));
                        recipe.setDescription(XmlParser.getValue(el, "description"));
                        recipes.add(recipe);
                    }
                }
            }
            return recipes;
        }

        @Override
        protected void onPostExecute(List<RecipeGroup> recipeGroups) {
            loader.setVisibility(View.GONE);

            recipeListAdapter = new RecipeListAdapter(recipeGroups, activity);
            recipeList.setAdapter(recipeListAdapter);
            recipeListAdapter.notifyDataSetChanged();
        }
    }

    public class RecipeListAdapter extends BaseExpandableListAdapter {

        private List<RecipeGroup> recipeGroups;
        private LayoutInflater inflater;

        public RecipeListAdapter(List<RecipeGroup> recipeGroups, Activity activity) {
            this.recipeGroups = recipeGroups;
            this.inflater = activity.getLayoutInflater();
        }

        @Override
        public int getGroupCount() {
            return recipeGroups.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return recipeGroups.get(groupPosition).getRecipes().size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return recipeGroups.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return recipeGroups.get(groupPosition).getRecipes().get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.recipe_list_group, null);
            }
            RecipeGroup recipeGroup = (RecipeGroup) getGroup(groupPosition);
            ((TextView) convertView.findViewById(R.id.recipeGroup)).setText(recipeGroup.getName());
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.recipe_list_item, null);
            }
            final Recipe recipe = (Recipe) getChild(groupPosition, childPosition);
            ((TextView) convertView.findViewById(R.id.recipeItem)).setText(recipe.getTitle());
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }
}
