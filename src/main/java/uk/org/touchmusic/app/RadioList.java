package uk.org.touchmusic.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.github.kevinsawicki.http.HttpRequest;
import com.squareup.picasso.Picasso;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import uk.org.touchmusic.app.model.ItemType;
import uk.org.touchmusic.app.model.RadioItem;

import java.util.ArrayList;
import java.util.List;

public class RadioList extends Fragment {

    private RadioListAdapter radioListAdadpter;
    private ListView radioListView;
    private TouchMusicApplication app;
    private ContentProvider contentProvider;
    private ProgressBar loader;
    private Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        app = (TouchMusicApplication) getActivity().getApplication();
        contentProvider = app.getContentProvider();

        View v = inflater.inflate(R.layout.radio_list, container, false);
        radioListView = (ListView) v.findViewById(R.id.radioListView);

        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            // list header image
            View header = LayoutInflater.from(getActivity()).inflate(R.layout.listview_header, null);
            ImageView headerImage = (ImageView) header.findViewById(R.id.list_header_image);
            headerImage.setImageResource(R.drawable.radio_header);
            radioListView.addHeaderView(header);
        }

        loader = (ProgressBar) v.findViewById(R.id.loader);
        loader.setVisibility(View.GONE);
        refreshData();
        return v;
    }

    public interface OnRadioItemSelected {
        public void onRadioItemSelected(int idx);
    }

    OnRadioItemSelected listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        try {
            listener = (OnRadioItemSelected) activity;
        } catch (ClassCastException ex) {
        }
    }

    public void refreshData() {
        new GetRadioItems().execute();
    }

    private class GetRadioItems extends AsyncTask<String, Void, List<RadioItem>> {

        private final String RADIO_URL = "http://www.touchmusic.org.uk/TouchPod/podcast.xml";

        public GetRadioItems() {
        }

        @Override
        protected void onPreExecute() {
            loader.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<RadioItem> doInBackground(String... params) {
            List<RadioItem> radioItems = new ArrayList<RadioItem>();
            radioItems = contentProvider.getContent(ItemType.RADIO.name(), radioItems.getClass());
            if (radioItems == null) {
                radioItems = doRequest(RADIO_URL);
            }
            return radioItems;
        }

        private List<RadioItem> doRequest(String url) {

            String response = HttpRequest.get(url).body();

            List<RadioItem> radioItems = new ArrayList<RadioItem>();
            Document xml = XmlParser.stringToXml(response);
            NodeList releases = xml.getElementsByTagName("item");

            for (int i = 0; i < releases.getLength(); i++) {
                Element el = (Element) releases.item(i);

                RadioItem radioItem = new RadioItem();
                radioItem.setId(i);
                radioItem.setTitle(XmlParser.getValue(el, "itunes:subtitle"));
                radioItem.setSubtitle(XmlParser.getValue(el, "title"));
                radioItem.setDescription(XmlParser.getValue(el, "description"));
                radioItem.setHighQualityMp3(XmlParser.getValue(el, "link"));
                radioItems.add(radioItem);
            }

            // cache results
            contentProvider.setContent(ItemType.RADIO.name(), radioItems);
            return radioItems;
        }

        @Override
        protected void onPostExecute(List<RadioItem> radioItems) {
            loader.setVisibility(View.GONE);

            radioListAdadpter = new RadioListAdapter(activity, 0, radioItems);
            radioListView.setAdapter(radioListAdadpter);
            radioListAdadpter.notifyDataSetChanged();
        }
    }

    public class RadioListAdapter extends ArrayAdapter<RadioItem> {

        private List<RadioItem> radioItems;
        private Context context;

        public RadioListAdapter(Context context, int resource, List<RadioItem> radioItems) {
            super(context, resource, radioItems);
            this.radioItems = radioItems;
            this.context = context;
        }

        @Override
        public int getCount() {
            return radioItems.size();
        }

        @Override
        public RadioItem getItem(int position) {
            return radioItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;

            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                convertView = inflater.inflate(R.layout.radio_list_item, null);
                viewHolder = new ViewHolder();
                viewHolder.coverArt = (ImageView) convertView.findViewById(R.id.coverArt);
                viewHolder.title = (TextView) convertView.findViewById(R.id.title);
                viewHolder.subtitle = (TextView) convertView.findViewById(R.id.subtitle);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final RadioItem radioItem = radioItems.get(position);
            Picasso.with(this.context)
                    .load(radioItem.getCoverArtUrl())
                    .placeholder(R.drawable.list_landscape_placeholder)
                    .resize(app.getPxFromDpi(80), app.getPxFromDpi(58))
                    //.resize(217, 175)
                    .into(viewHolder.coverArt);
            viewHolder.title.setText(radioItem.getTitle());
            viewHolder.subtitle.setText(radioItem.getSubtitle());
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRadioItemSelected(position);
                }
            });
            return convertView;
        }
    }

    static class ViewHolder {
        ImageView coverArt;
        TextView title;
        TextView subtitle;
    }
}
