package uk.org.touchmusic.app;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.util.DisplayMetrics;
import android.view.*;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import uk.org.touchmusic.app.model.FlickrPhoto;
import uk.org.touchmusic.app.model.ItemType;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Fragment to display a single photo
 */
public class PhotoDetail extends Fragment implements DownloadPhotoTask.Listener {

    private TouchMusicApplication app;
    private ContentProvider contentProvider;
    private ProgressBar loader;
    private String photoUrl;
    private String shareSubject;
    private String shareText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.photo_detail, container, false);

        setHasOptionsMenu(true);

        app = (TouchMusicApplication) getActivity().getApplication();
        contentProvider = app.getContentProvider();

        int idx = getArguments().getInt("idx");

        List<FlickrPhoto> photos = new ArrayList<FlickrPhoto>();
        photos = contentProvider.getContent(ItemType.PHOTOS.name(), photos.getClass());
        // use a smaller image for <= hpdi
        if (app.getDevice().getDensityDpi() > DisplayMetrics.DENSITY_HIGH) {
            photoUrl = photos.get(idx).medium800();
        } else {
            photoUrl = photos.get(idx).medium500();
        }

        loader = (ProgressBar) rootView.findViewById(R.id.loader);
        loader.bringToFront();

        TouchImageView photo = (TouchImageView) rootView.findViewById(R.id.photo);
        photo.setMaxZoom(4);

        // toggle actionbar
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
                if (actionBar.isShowing()) {
                    actionBar.hide();
                } else {
                    actionBar.show();
                }
            }
        });

        Picasso.with(getActivity().getBaseContext())
                .load(photoUrl)
                .into(photo, new Callback() {
                    @Override
                    public void onSuccess() {
                        loader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        loader.setVisibility(View.GONE);
                    }
                });

        registerForContextMenu(rootView);

        // share the link to the photo
        shareSubject = "Touch Photos";
        shareText = "Touch Photos - " + photoUrl;

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.photo_detail, menu);
        MenuItem shareItem = menu.findItem(R.id.menu_item_share);
        ShareActionProvider shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, shareSubject);
        intent.putExtra(Intent.EXTRA_TEXT, shareText);
        shareActionProvider.setShareIntent(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save:
                if (hasSDCardMounted()) {
                    new DownloadPhotoTask(getActivity(), this, getStoragePath()).execute(photoUrl);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean hasSDCardMounted() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    public String getStoragePath() {
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/TouchPhotos/");
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        return storageDir.getPath();
    }

    @Override
    public void onPhotoDownloadComplete(Integer statusCode) {
        switch (statusCode) {
            case DownloadPhotoTask.SUCCESS:
                Toast.makeText(getActivity(), "Photo downloaded", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
