package uk.org.touchmusic.app.model;

import java.util.ArrayList;
import java.util.List;

public class RecipeGroup {
    private String name;
    private List<Recipe> recipes = new ArrayList<Recipe>();

    public RecipeGroup(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }
}
