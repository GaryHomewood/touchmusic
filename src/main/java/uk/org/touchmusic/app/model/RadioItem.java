package uk.org.touchmusic.app.model;

public class RadioItem {
    private int id;
    private String title;
    private String subtitle;
    private String description;
    private String coverArtUrl;
    private String highQualityMp3;
    private String normalQualityMp3;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHighQualityMp3() {
        return highQualityMp3;
    }

    public void setHighQualityMp3(String highQualityMp3) {
        this.highQualityMp3 = highQualityMp3;
    }

    public String getNormalQualityMp3() {
        return highQualityMp3.replace("touchradio", "touchiphoneradio");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getCoverArtUrl() {
        return highQualityMp3.replace("radio/", "radio/images/").replace("mp3", "jpg");
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
