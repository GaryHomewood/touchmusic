package uk.org.touchmusic.app.model;

/**
 * An enum for the types of item displayed in the app. Also used as key in the cache
 */
public enum  ItemType {
    NEWS,
    PHOTOS,
    CATALOGUE,
    RADIO,
    RECIPE
}
