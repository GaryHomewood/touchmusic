package uk.org.touchmusic.app.model;

/**
 * User: Gary Homewood
 * Date: 04/12/2013
 * Time: 15:00
 */
public class Recipe {
    private String title;
    private String excerpt;
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
