package uk.org.touchmusic.app.model;

/**
 * User: Gary Homewood
 * Date: 14/11/2013
 * Time: 11:49
 */
public class NewsItem {
    private String title;
    private String pubDate;
    private String description;
    private String link;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getDescription() {
        return description.replaceAll("<iframe.+/(iframe)*>", "");
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
