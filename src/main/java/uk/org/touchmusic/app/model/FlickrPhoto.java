package uk.org.touchmusic.app.model;

import java.sql.Timestamp;

public class FlickrPhoto implements Comparable<FlickrPhoto> {
	private final String id;
    private final String secret;
    private final String server;
    private final String farm;
    private final Timestamp dateUpload;

	public FlickrPhoto(String id, String secret, String server, String farm, long dateUpload) {
		this.id = id;
		this.secret = secret;
		this.server = server;
		this.farm = farm;
        this.dateUpload = new Timestamp(dateUpload);
    }

    @Override
    public int compareTo(FlickrPhoto another) {
        return (this.dateUpload.after(another.dateUpload)) ? -1 : 1;
    }

    public String smallSquare75() {
		return url("_s");
	}

    public String largeSquare150() {
        return url("_q");
    }

    public String small240() {
        return url("_m");
    }

    public String small320() {
        return url("_n");
    }

    public String medium500() {
        return url("");
    }

    public String medium640() {
        return url("_z");
    }

    public String medium800() {
        return url("_c");
    }

    public String large1024() {
        return url("_b");
	}

    /**
     * Flickr photo source url, includes a size suffix
     * see <link>http://www.flickr.com/services/api/misc.urls.html</link>
     * @param suffix specifies the size of the image to return
     * @return the url to the image
     */
    private String url(String suffix) {
        return "http://farm" + farm + ".static.flickr.com/" + server + "/" + id + "_" + secret + suffix + ".jpg";
    }
}