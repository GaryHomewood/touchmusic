package uk.org.touchmusic.app.model;

public class CatalogueItem {

    private String artist;
    private String title;
    private String releaseDate;
    private String catalogueNumber;
    private String coverArtUrl;
    private String trackListing;
    private String mp3sampleUrl;
    private String description;
    private String releaseUrl;

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getCatalogueNumber() {
        return catalogueNumber;
    }

    public void setCatalogueNumber(String catalogueNumber) {
        this.catalogueNumber = catalogueNumber;
    }

    public String getCoverArtUrl() {
        return coverArtUrl;
    }

    public void setCoverArtUrl(String coverArtUrl) {
        this.coverArtUrl = coverArtUrl;
    }

    public String getTrackListing() {
        return trackListing;
    }

    public void setTrackListing(String trackListing) {
        this.trackListing = trackListing;
    }

    public String getMp3sampleUrl() {
        return mp3sampleUrl;
    }

    public void setMp3sampleUrl(String mp3sampleUrl) {
        this.mp3sampleUrl = mp3sampleUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReleaseUrl() {
        return releaseUrl;
    }

    public void setReleaseUrl(String releaseUrl) {
        this.releaseUrl = releaseUrl;
    }
}
