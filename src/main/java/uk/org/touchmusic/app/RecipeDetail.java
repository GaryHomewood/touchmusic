package uk.org.touchmusic.app;

import android.os.Bundle;
import android.text.Html;
import android.view.*;
import android.webkit.WebView;
import android.widget.TextView;

public class RecipeDetail extends BaseDetailFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.recipe_item, container, false);

        String recipeName = getArguments().getString("excerpt");
        String by = "by " + getArguments().getString("title");
        String recipe = getArguments().getString("description");

        TextView excerpt = (TextView) v.findViewById(R.id.excerpt);
        TextView title = (TextView) v.findViewById(R.id.title);

        excerpt.setText(recipeName);
        title.setText(by);

        WebView wv = (WebView) v.findViewById(R.id.description);
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head><link href=\"styles.css\" type=\"text/css\" rel=\"stylesheet\"/></head><body>");
        sb.append(recipe);
        sb.append("</body></html>");
        wv.loadDataWithBaseURL("file:///android_asset/", sb.toString(), "text/html", "utf-8", null);

        // share the recipe as text (with image tags removed)
        setHasOptionsMenu(true);
        shareSubject = recipeName + " " + by;
        String shareableRecipe = recipe.replaceAll("<img.+/(img)*>", "").replaceAll("<img.+?>", "");
        shareText = Html.fromHtml(shareSubject + " " + shareableRecipe, null, null).toString();
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }
}
