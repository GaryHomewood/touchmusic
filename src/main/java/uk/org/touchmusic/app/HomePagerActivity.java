package uk.org.touchmusic.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;
import uk.org.touchmusic.app.model.ItemType;
import uk.org.touchmusic.app.model.Recipe;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class HomePagerActivity extends ActionBarActivity
        implements NewsList.OnNewsItemSelectedListener,
        PhotoList.OnPhotoSelectedListener,
        CatalogueList.OnCatalogueItemSelected,
        RadioList.OnRadioItemSelected,
        RecipeList.OnRecipeSelected,
        Player.OnPlayerRemove {

    private static int selectedPage;

    private TouchMusicApplication app;
    private ContentProvider contentProvider;
    private ViewPager pager;
    private HomePagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_pager);

        app = (TouchMusicApplication) getApplication();
        contentProvider = app.getContentProvider();

        adapter = new HomePagerAdapter(this, getSupportFragmentManager());
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // restore to the selected page
        if (savedInstanceState != null) {
            selectedPage = savedInstanceState.getInt("selectedPage");
        }
        pager.setCurrentItem(selectedPage, true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // ensure back from a detail page returns to the originally selected page
        selectedPage = pager.getCurrentItem();
        outState.putInt("selectedPage", selectedPage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                if (app.isOnline()) {
                    // clear cache and re-retrieve data for the current page displayed in the view pager
                    contentProvider.clearContent(adapter.getItemType(selectedPage));
                    selectedPage = pager.getCurrentItem();
                    Fragment fragment = adapter.getFragment(selectedPage);

                    switch (selectedPage) {
                        case 0:
                            ((NewsList) fragment).refreshData();
                            break;
                        case 1:
                            ((PhotoList) fragment).refreshData();
                            break;
                        case 2:
                            ((CatalogueList) fragment).refreshData();
                            break;
                        case 3:
                            ((RadioList) fragment).refreshData();
                            break;
                        case 4:
                            ((RecipeList) fragment).refreshData();
                            break;
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_network), Toast.LENGTH_SHORT).show();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNewsItemSelected(int position) {
        Intent i = new Intent(getApplicationContext(), DetailActivity.class);
        i.putExtra("idx", position);
        i.putExtra("itemType", "news");
        startActivity(i);
    }

    @Override
    public void onPhotoSelected(int position) {
        Intent i = new Intent(getApplicationContext(), PhotoDetailPager.class);
        i.putExtra("idx", position);
        i.putExtra("itemType", "photo");
        startActivity(i);
    }

    @Override
    public void onCatalogueItemSelected(int idx) {
        Intent i = new Intent(getApplicationContext(), DetailActivity.class);
        i.putExtra("idx", idx);
        i.putExtra("itemType", "catalogue");
        startActivity(i);
    }

    @Override
    public void onRadioItemSelected(int idx) {
        Intent i = new Intent(getApplicationContext(), DetailActivity.class);
        i.putExtra("idx", idx);
        i.putExtra("itemType", "radio");
        startActivity(i);
    }

    @Override
    public void onRecipeSelected(Recipe recipe) {
        Intent i = new Intent(getApplicationContext(), DetailActivity.class);
        i.putExtra("title", recipe.getTitle());
        i.putExtra("excerpt", recipe.getExcerpt());
        i.putExtra("description", recipe.getDescription());
        i.putExtra("itemType", "recipe");
        startActivity(i);
    }

    @Override
    public void onPlayerRemove() {
        // if the player has been dismissed...
        app.removeCurrentlyPlaying();
        app.getPlayerService().stop();
        FragmentManager fm = getSupportFragmentManager();
        Fragment f = fm.findFragmentByTag("LIST_PLAYER");
        fm.beginTransaction().remove(f).commit();
        (findViewById(R.id.playerContainer)).setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();

        // show the player if an mp3 is playing
        FragmentManager fm = getSupportFragmentManager();
        FrameLayout fl = (FrameLayout) findViewById(R.id.playerContainer);

        if (!TextUtils.isEmpty(app.getCurrentlyPlaying())) {
            Player player = Player.getInstance(app.getCurrentlyPlayingType(), app.getCurrentlyPlayingIdx());
            fm.beginTransaction().replace(R.id.playerContainer, player, "LIST_PLAYER").commit();
            fl.setVisibility(View.VISIBLE);
        } else {
            fl.setVisibility(View.GONE);
        }
    }

    public class HomePagerAdapter extends FragmentStatePagerAdapter {
        private Context context;
        private NewsList newsList = null;
        private PhotoList photoList = null;
        private CatalogueList catalogueList = null;
        private RadioList radioList = null;
        private RecipeList recipeList = null;

        public HomePagerAdapter(Context context, FragmentManager fragmentManager) {
            super(fragmentManager);
            this.context = context;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    if (newsList == null) {
                        newsList = new NewsList();
                    }
                    return newsList;
                case 1:
                    if (photoList == null) {
                        photoList = new PhotoList();
                    }
                    return photoList;
                case 2:
                    if (catalogueList == null) {
                        catalogueList = new CatalogueList();
                    }
                    return catalogueList;
                case 3:
                    if (radioList == null) {
                        radioList = new RadioList();
                    }
                    return radioList;
                case 4:
                    if (recipeList == null) {
                        recipeList = new RecipeList();
                    }
                    return recipeList;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "NEWS";
                case 1:
                    return "PHOTOS";
                case 2:
                    return "CATALOGUE";
                case 3:
                    return "RADIO";
                case 4:
                    return "RECIPES";
            }
            return null;
        }

        public ItemType getItemType(int page) {
            ItemType itemType = null;
            switch (page) {
                case 0:
                    itemType = ItemType.NEWS;
                    break;
                case 1:
                    itemType = ItemType.PHOTOS;
                    break;
                case 2:
                    itemType = ItemType.CATALOGUE;
                    break;
                case 3:
                    itemType = ItemType.RADIO;
                    break;
                case 4:
                    itemType = ItemType.RECIPE;
                    break;
            }
            return itemType;
        }

        /**
         * orientation change means activity is recreated, and FragmentStatePagerAdapter restores
         * fragments from the FragmentManager without calling getItem. use reflection to get a reference
         * to the fragment
         *
         * @param position of the fragment
         * @return the fragment
         */
        @SuppressWarnings("unchecked")
        public Fragment getFragment(int position) {
            try {
                Field f = FragmentStatePagerAdapter.class.getDeclaredField("mFragments");
                f.setAccessible(true);
                ArrayList<Fragment> fragments = (ArrayList<Fragment>) f.get(this);
                if (fragments.size() > position) {
                    return fragments.get(position);
                }
                return null;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
