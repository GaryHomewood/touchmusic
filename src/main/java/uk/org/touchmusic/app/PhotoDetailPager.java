package uk.org.touchmusic.app;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import uk.org.touchmusic.app.model.FlickrPhoto;
import uk.org.touchmusic.app.model.ItemType;

import java.util.ArrayList;
import java.util.List;

import static android.support.v4.app.Fragment.instantiate;

/**
 * Fragment activity with an action bar to provide a paged view of single photos
 */
public class PhotoDetailPager extends ActionBarActivity {

    private ViewPager pager;
    private PagerAdapter pagerAdapter;
    private TouchMusicApplication app;
    private ContentProvider contentProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_detail_pager);

        app = (TouchMusicApplication) getApplication();
        contentProvider = app.getContentProvider();

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.argb(128, 0, 0, 0)));

        Bundle extras = getIntent().getExtras();
        int idx = extras.getInt("idx");

        List<FlickrPhoto> photos = new ArrayList<FlickrPhoto>();
        photos = contentProvider.getContent(ItemType.PHOTOS.name(), photos.getClass());

        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new PhotoPagerAdapter(getSupportFragmentManager(), photos, this);
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(idx);
    }

    private class PhotoPagerAdapter extends FragmentStatePagerAdapter {

        private List<FlickrPhoto> images;
        private Context context;

        public PhotoPagerAdapter(FragmentManager fm, List<FlickrPhoto> images, Context context) {
            super(fm);
            this.images = images;
            this.context = context;
        }

        @Override
        public Fragment getItem(int position) {
            Bundle extras = new Bundle();
            extras.putInt("idx", position);
            return instantiate(this.context, PhotoDetail.class.getName(), extras);
        }

        @Override
        public int getCount() {
            return images.size();
        }
    }
}
