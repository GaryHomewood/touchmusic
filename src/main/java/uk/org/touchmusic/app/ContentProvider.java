package uk.org.touchmusic.app;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import uk.org.touchmusic.app.model.*;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Using SharedPreferences as a cache, since some of the http requests don't implement eTag
 */
public class ContentProvider {
    private static ContentProvider contentProvider;
    private static Gson gson = new Gson();

    private final Context context;
    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor sharedPreferencesEditor;

    private ContentProvider(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        sharedPreferencesEditor = sharedPreferences.edit();
    }

    public static ContentProvider getInstance(Context context) {
        if (contentProvider == null) {
            contentProvider = new ContentProvider(context);
        }
        return contentProvider;
    }

    public void setContent(String key, Object object) {
        sharedPreferencesEditor.putString(key, gson.toJson(object));
        sharedPreferencesEditor.commit();
    }

    public <T> T getContent(String key, Class<T> o) {
        String json = sharedPreferences.getString(key, null);
        if (json != null) {
            try {
                return gson.fromJson(json, getType(key));
            } catch (Exception ex) {
            }
        }
        return null;
    }

    public void clearContent(ItemType itemType) {
        setContent(itemType.name(), null);
    }

    /**
     * Type is tightly coupled to the key
     * @param key the key of the collection of objects
     * @return the Type to get gson to use
     */
    private Type getType(String key) {
        if (key.equals(ItemType.NEWS.name())) {
            return new TypeToken<List<NewsItem>>(){}.getType();
        } else if (key.equals(ItemType.PHOTOS.name())) {
            return new TypeToken<List<FlickrPhoto>>(){}.getType();
        } else if (key.equals(ItemType.CATALOGUE.name())) {
            return new TypeToken<List<CatalogueItem>>(){}.getType();
        } else if (key.equals(ItemType.RADIO.name())) {
            return new TypeToken<List<RadioItem>>(){}.getType();
        } else if (key.equals(ItemType.RECIPE.name())) {
            return new TypeToken<List<RecipeGroup>>(){}.getType();
        }
        return null;
    }
}
