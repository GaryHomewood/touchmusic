package uk.org.touchmusic.app;

import android.util.DisplayMetrics;

/**
 * Device related details
 */
public class Device {
    private DisplayMetrics metrics;

    public Device(DisplayMetrics metrics) {
        this.metrics = metrics;
    }

    /**
     * @return  320 for xhdpi
     *          240 for hdpi
     *          160 for mdpi
     */
    public int getDensityDpi() {
        return metrics.densityDpi;
    }
}
