package uk.org.touchmusic.app;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.util.TypedValue;
import uk.org.touchmusic.app.model.*;

import java.util.List;

/**
 * User: Gary Homewood
 * Date: 14/11/2013
 * Time: 17:20
 */
public class TouchMusicApplication extends Application {

    private Device device;
    private ContentProvider contentProvider = null;

    private List<NewsItem> newsItems;
    private List<FlickrPhoto> photos;
    private List<CatalogueItem> catalogueItems;
    private List<RadioItem> radioItems;

    // store details of currently playing catalogue sample or radio mp3
    private String currentlyPlaying = "";
    private ItemType currentlyPlayingType;
    private int currentlyPlayingIdx;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        device = new Device(getResources().getDisplayMetrics());
        contentProvider = ContentProvider.getInstance(getBaseContext());
        Intent serviceIntent = new Intent(this, PlayerService.class);
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private PlayerService playerService;
    private boolean serviceBound = false;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            playerService = ((PlayerService.PlayerBinder)service).getService();
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };

    @Override
    public void onTerminate() {
        super.onTerminate();
        if (serviceBound) {
            unbindService(serviceConnection);
            serviceBound = false;
        }
    }

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public PlayerService getPlayerService() {
        return playerService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }

    public Device getDevice() {
        return device;
    }

    public ContentProvider getContentProvider() {
        return contentProvider;
    }

    public String getCurrentlyPlaying() {
        return currentlyPlaying;
    }

    public void setCurrentlyPlaying(String mp3, ItemType itemType, int idx) {
        this.currentlyPlaying = mp3;
        this.currentlyPlayingType = itemType;
        this.currentlyPlayingIdx = idx;
    }

    public void removeCurrentlyPlaying() {
        this.currentlyPlaying = "";
        this.currentlyPlayingType = null;
        this.currentlyPlayingIdx = 0;
    }

    public ItemType getCurrentlyPlayingType() {
        return currentlyPlayingType;
    }

    public int getCurrentlyPlayingIdx() {
        return currentlyPlayingIdx;
    }

    public int getPxFromDpi(int dpi) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) dpi, getResources().getDisplayMetrics());
    }
}
