package uk.org.touchmusic.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import uk.org.touchmusic.app.model.ItemType;
import uk.org.touchmusic.app.model.RadioItem;

import java.util.ArrayList;
import java.util.List;

public class RadioDetail extends Fragment {

    private TouchMusicApplication app;
    private ContentProvider contentProvider;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.radio_item, container, false);

        app = (TouchMusicApplication) getActivity().getApplication();
        contentProvider = app.getContentProvider();

        final int idx = getArguments().getInt("idx");
        final String key = ItemType.NEWS.name() + idx;

        List<RadioItem> radioItems = new ArrayList<RadioItem>();
        radioItems = contentProvider.getContent(ItemType.RADIO.name(), radioItems.getClass());
        final RadioItem radioItem = radioItems.get(idx);
        final String mp3 = radioItem.getNormalQualityMp3();

        TextView subtitle = (TextView) v.findViewById(R.id.subtitle);
        TextView title = (TextView) v.findViewById(R.id.title);
        ImageView coverArt = (ImageView) v.findViewById(R.id.coverArt);
        TextView description = (TextView) v.findViewById(R.id.description);

        // TODO hide play button if mp3 gives a 404
        Button playButton = (Button) v.findViewById(R.id.playButton);

        title.setText(radioItem.getTitle());
        subtitle.setText(radioItem.getSubtitle());
        Picasso.with(getActivity().getBaseContext())
                .load(radioItem.getCoverArtUrl())
                .into(coverArt);
        description.setText(radioItem.getDescription());

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FrameLayout fl = (FrameLayout) getActivity().findViewById(R.id.playerContainer);
                Player player = (Player) fm.findFragmentByTag("DETAIL_PLAYER");

                // play mp3 and show details
                if (player == null || !player.getKey().equals(key)) {
                    player = Player.getInstance(ItemType.NEWS, idx);
                    fm.beginTransaction().replace(R.id.playerContainer, player, "DETAIL_PLAYER").setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
                    app.getPlayerService().play(mp3, radioItem.getSubtitle(), radioItem.getTitle());
                    app.setCurrentlyPlaying(mp3, ItemType.NEWS, idx);
                }

                fl.setVisibility(View.VISIBLE);
            }
        });

        return v;
    }
}
