package uk.org.touchmusic.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import uk.org.touchmusic.app.model.CatalogueItem;
import uk.org.touchmusic.app.model.ItemType;
import uk.org.touchmusic.app.model.RadioItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Player footer panel for a catalogue sample or a radio item
 */
public class Player extends Fragment {

    private ImageView playbackControl;
    private ImageView coverArt;
    private TextView title;
    private TextView subtitle;
    private ImageView playbackRemove;
    private TouchMusicApplication app;
    private ContentProvider contentProvider;
    private RadioItem radioItem = null;
    private CatalogueItem catalogueItem = null;
    private ItemType itemType;
    private String itemTypeName;
    private int idx;
    private String coverArtUrl;
    private String mp3;
    private String mp3Title = "";
    private String mp3Subtitle = "";

    /**
     * playing mp3 identified by type + index
     */
    public static Player getInstance(ItemType itemType, int idx) {
        Player p = new Player();
        Bundle args = new Bundle();
        args.putString("itemTypeName", itemType.name());
        args.putInt("idx", idx);
        p.setArguments(args);
        return p;
    }

    public String getKey() {
        return itemTypeName + idx;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.player, container, false);

        app = (TouchMusicApplication) getActivity().getApplication();
        contentProvider = app.getContentProvider();

        itemTypeName = getArguments().getString("itemTypeName");
        idx = getArguments().getInt("idx");

        playbackControl = (ImageView) v.findViewById(R.id.playbackControl);
        coverArt = (ImageView) v.findViewById(R.id.coverArt);
        title = (TextView) v.findViewById(R.id.title);
        subtitle = (TextView) v.findViewById(R.id.subtitle);
        playbackRemove = (ImageView) v.findViewById(R.id.playbackRemove);

        // playing a radio mp3
        if (itemTypeName.equals(ItemType.NEWS.name())) {
            itemType = ItemType.NEWS;
            List<RadioItem> radioItems = new ArrayList<RadioItem>();
            radioItems = contentProvider.getContent(ItemType.RADIO.name(), radioItems.getClass());
            radioItem = radioItems.get(idx);

            coverArtUrl = radioItem.getCoverArtUrl();
            mp3 = radioItem.getNormalQualityMp3();
            mp3Title = radioItem.getTitle();
            mp3Subtitle = radioItem.getSubtitle();
        }

        // playing a catalogue sample mp3
        if (itemTypeName.equals(ItemType.CATALOGUE.name())) {
            itemType = ItemType.CATALOGUE;
            List<CatalogueItem> catalogueItems = new ArrayList<CatalogueItem>();
            catalogueItems = contentProvider.getContent(ItemType.CATALOGUE.name(), catalogueItems.getClass());
            catalogueItem = catalogueItems.get(idx);

            coverArtUrl = catalogueItem.getCoverArtUrl();
            mp3 = catalogueItem.getMp3sampleUrl();
            mp3Title = catalogueItem.getArtist();
            mp3Subtitle = catalogueItem.getTitle();
        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!TextUtils.isEmpty(app.getCurrentlyPlaying())) {
            showItem();
        }
    }

    /**
     * Show details of currently playing audio
     */
    public void showItem() {

        playbackControl.setVisibility(View.VISIBLE);
        if (app.getPlayerService().isPlaying()) {
            playbackControl.setImageResource(R.drawable.ic_action_pause);
        } else {
            playbackControl.setImageResource(R.drawable.ic_action_play);
        }

        playbackControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (app.getPlayerService().isPlaying()) {
                    app.getPlayerService().pause();
                    playbackControl.setImageResource(R.drawable.ic_action_play);
                } else {
                    String mp3currentlyPlaying = app.getCurrentlyPlaying();
                    if (app.getPlayerService().isPaused() && mp3currentlyPlaying.equals(mp3)) {
                        app.getPlayerService().resume();
                    } else {
                        app.getPlayerService().play(mp3, mp3Title, mp3Subtitle);
                    }
                    playbackControl.setImageResource(R.drawable.ic_action_pause);
                    app.setCurrentlyPlaying(mp3, itemType, idx);
                }
            }
        });

        Picasso.with(getActivity())
                .load(coverArtUrl)
                .placeholder(R.drawable.list_square_placeholder)
                .resize(app.getPxFromDpi(58), app.getPxFromDpi(58))
                .centerCrop()
                .into(coverArt);

        title.setText(mp3Title);
        subtitle.setText(mp3Subtitle);

        playbackRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPlayerRemove();
            }
        });
    }

    private OnPlayerRemove listener;

    public interface OnPlayerRemove {
        public void onPlayerRemove();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (OnPlayerRemove) activity;
        } catch (ClassCastException ex) {}
    }
}
