package uk.org.touchmusic.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import java.io.IOException;

public class PlayerService extends Service {

    private static int classID = 579;

    private MediaPlayer mediaPlayer;
    private int position = 0;
    private String filePath;
    private String title;
    private String artist;

    private enum State {
        Retrieving,
        Stopped,
        Preparing,
        Playing,
        Paused
    }

    private State state = State.Retrieving;

    public class PlayerBinder extends Binder {
        PlayerService getService() {
            return PlayerService.this;
        }
    }

    private final IBinder binder = new PlayerBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
       return Service.START_STICKY;
    }

    private NotificationManager notificationManager;

    public  void play(String filePath, final String title, final String artist) {
        state = State.Retrieving;

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        this.filePath = filePath;
        this.title = title;
        this.artist = artist;

        try {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }

            mediaPlayer = new MediaPlayer();
            mediaPlayer.reset();
            mediaPlayer.setVolume(1.0f, 1.0f);
            mediaPlayer.setDataSource(filePath);
            mediaPlayer.prepareAsync();

            final PlayerService ctx = this;

            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer arg0) {
                    state = State.Playing;

                    // show a notification for the currently playimg mp3
                    Intent intent = new Intent(ctx, HomePagerActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    PendingIntent pi = PendingIntent.getActivity(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                    Notification notification = new NotificationCompat.Builder(getApplicationContext())
                            .setSmallIcon(R.drawable.icon)
                            .setContentTitle(artist)
                            .setContentText(title)
                            .setContentIntent(pi)
                            .setPriority(Notification.PRIORITY_LOW)
                            .build();

                    notificationManager.notify(1, notification);

                    mediaPlayer.start();
                }
            });

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.reset();
                    mediaPlayer.release();
                    mediaPlayer = null;
                    position = 0;
                    notificationManager.cancelAll();
                    state = State.Stopped;
                }
            });

        } catch (IOException e) {
        }
    }

    @Override
    public void onDestroy() {
        stop();
    }

    public void pause() {
        // only pause if its already playing
        if (state == State.Playing) {
            state = State.Paused;
            if (mediaPlayer != null) {
                position = mediaPlayer.getCurrentPosition();
                mediaPlayer.pause();
            }
        }
    }

    public void resume() {
        if (state == State.Stopped) {
            play(this.filePath, this.title, this.artist);
        } else {
            if (position > 0) {
                mediaPlayer.seekTo(position);
            }
            if (state != State.Playing) {
                mediaPlayer.start();
            }
            state = State.Playing;
        }
    }

    public void stop() {
        if (state == State.Playing) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        notificationManager.cancelAll();
    }

    public boolean isPlaying() {
        return (state == State.Retrieving || state == State.Playing);
    }

    public boolean isPaused() {
        return (state != State.Playing);
    }
}
