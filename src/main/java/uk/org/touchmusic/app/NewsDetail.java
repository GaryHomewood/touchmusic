package uk.org.touchmusic.app;

import android.os.Bundle;
import android.view.*;
import android.webkit.WebView;
import android.widget.TextView;
import uk.org.touchmusic.app.model.ItemType;
import uk.org.touchmusic.app.model.NewsItem;

import java.util.ArrayList;
import java.util.List;

public class NewsDetail extends BaseDetailFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.news_item, container, false);

        int idx = getArguments().getInt("idx");

        List<NewsItem> newsItems = new ArrayList<NewsItem>();
        newsItems = contentProvider.getContent(ItemType.NEWS.name(), newsItems.getClass());
        NewsItem newsItem = newsItems.get(idx);

        TextView title = (TextView) v.findViewById(R.id.title);
        TextView pubDate = (TextView) v.findViewById(R.id.pubDate);

        title.setText(newsItem.getTitle());
        pubDate.setText(newsItem.getPubDate());

        WebView wv = (WebView) v.findViewById(R.id.description);
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head><link href=\"styles.css\" type=\"text/css\" rel=\"stylesheet\"/></head><body>");
        sb.append(newsItem.getDescription());
        sb.append("</body></html>");
        wv.loadDataWithBaseURL("file:///android_asset/", sb.toString(), "text/html", "utf-8", null);

        // share a link to the blog post
        setHasOptionsMenu(true);
        shareSubject = newsItem.getTitle();
        shareText = newsItem.getTitle() + " - " + newsItem.getLink();
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }
}
