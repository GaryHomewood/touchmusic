package uk.org.touchmusic.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.github.kevinsawicki.http.HttpRequest;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import uk.org.touchmusic.app.model.ItemType;
import uk.org.touchmusic.app.model.NewsItem;

import java.util.ArrayList;
import java.util.List;

/**
 * User: garhom
 * Date: 11/11/2013
 * Time: 20:24
 */
public class NewsList extends Fragment {

    private NewsListAdapter newsListAdapter;
    private ListView newsListView;
    private TouchMusicApplication app;
    private ContentProvider contentProvider;
    private Activity activity;
    private ProgressBar loader;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        app = (TouchMusicApplication) getActivity().getApplication();
        contentProvider = app.getContentProvider();

        View v = inflater.inflate(R.layout.news_list, container, false);

        newsListView = (ListView) v.findViewById(R.id.newsListView);

        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            View header = LayoutInflater.from(getActivity()).inflate(R.layout.listview_header, null);
            ImageView headerImage = (ImageView) header.findViewById(R.id.list_header_image);
            headerImage.setImageResource(R.drawable.news_header);
            newsListView.addHeaderView(header);
        }

        loader = (ProgressBar) v.findViewById(R.id.loader);
        loader.setVisibility(View.GONE);

        refreshData();
        return v;
    }

    private OnNewsItemSelectedListener listener;

    public interface OnNewsItemSelectedListener {
        public void onNewsItemSelected(int position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        try {
            listener = (OnNewsItemSelectedListener) activity;
        } catch (ClassCastException ex) {
        }
    }

    public void refreshData() {
        new GetNews().execute();
    }

    private class GetNews extends AsyncTask<String, Void, List<NewsItem>> {

        private String NEWS_URL = "http://www.touchmusic.org.uk/iphone.xml";
        private String eTag = "";

        public GetNews() {
        }

        @Override
        protected void onPreExecute() {
            loader.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<NewsItem> doInBackground(String... params) {
            List<NewsItem> newsItems = new ArrayList<NewsItem>();
            if (TextUtils.isEmpty(eTag)) {
                // first time, do a request
                newsItems = doRequest(NEWS_URL);
            } else {
                boolean unchanged = HttpRequest.get(NEWS_URL).ifNoneMatch(eTag).notModified();
                if (unchanged) {
                    newsItems = contentProvider.getContent(ItemType.NEWS.name(), newsItems.getClass());
                } else {
                    newsItems = doRequest(NEWS_URL);
                }
            }
            return newsItems;
        }

        private List<NewsItem> doRequest(String url) {
            List<NewsItem> newsItems = new ArrayList<NewsItem>();
            HttpRequest request = HttpRequest.get(url);
            eTag = request.eTag();

            if (eTag != null) {
                String response = request.body();
                Document xml = XmlParser.stringToXml(response);
                NodeList items = xml.getElementsByTagName("item");

                for (int i = 0; i < items.getLength(); i++) {
                    Element el = (Element) items.item(i);

                    NewsItem newsItem = new NewsItem();
                    newsItem.setTitle(XmlParser.getValue(el, "title"));
                    newsItem.setPubDate(XmlParser.getValue(el, "pubDate"));
                    newsItem.setDescription(XmlParser.getValue(el, "description"));
                    newsItem.setLink(XmlParser.getValue(el, "link"));
                    newsItems.add(newsItem);
                }

                contentProvider.setContent(ItemType.NEWS.name(), newsItems);
            } else {
                // no network
                newsItems = contentProvider.getContent(ItemType.NEWS.name(), newsItems.getClass());
            }
            return newsItems;
        }

        @Override
        protected void onPostExecute(List<NewsItem> newsItems) {
            loader.setVisibility(View.GONE);

            newsListAdapter = new NewsListAdapter(activity, 0, newsItems);
            newsListView.setAdapter(newsListAdapter);
            newsListAdapter.notifyDataSetChanged();
        }
    }

    public class NewsListAdapter extends ArrayAdapter<NewsItem> {
        private Context context;
        private List<NewsItem> newsItems;

        public NewsListAdapter(Context context, int resource, List<NewsItem> newsItems) {
            super(context, resource, newsItems);
            this.newsItems = newsItems;
            this.context = context;
        }

        @Override
        public int getCount() {
            return newsItems.size();
        }

        @Override
        public NewsItem getItem(int position) {
            return newsItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;

            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();

                convertView = inflater.inflate(R.layout.news_list_item, null);
                viewHolder = new ViewHolder();
                viewHolder.title = (TextView) convertView.findViewById(R.id.title);
                viewHolder.pubDate = (TextView) convertView.findViewById(R.id.pubDate);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final NewsItem newsItem = newsItems.get(position);

            viewHolder.title.setText(newsItem.getTitle());
            viewHolder.pubDate.setText(newsItem.getPubDate());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onNewsItemSelected(position);
                }
            });

            return convertView;
        }
    }

    static class ViewHolder {
        TextView title;
        TextView pubDate;
    }
}
